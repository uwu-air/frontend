/* eslint-disable */

module.exports = {
	css: {
		loaderOptions: {
			sass: {
				prependData: `@import "@/assets/css/styles.scss";`
			},
		},
	},
	// devServer: {
	// 	proxy: {
	// 		'^/api': {
	// 			target: 'http://localhost:5000',
	// 			changeOrigin: true,
	// 			pathRewrite: { "^/api": "/" }
	// 		},
	// 	}
	// }
};
