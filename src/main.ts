import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import 'bootstrap/scss/bootstrap.scss';
import 'bootstrap-icons/font/bootstrap-icons.css';
import 'bootstrap';

createApp(App)
	.use(store)
	.use(router)
	.mixin({
		created() {
			document.title = 'UWU Virtual Airlines';
		},
	})
	.mount('#app');
